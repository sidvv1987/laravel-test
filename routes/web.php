<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SessionsController;
use App\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});


Route::get('register', [RegistrationController::class, 'register']);
Route::post('register', [RegistrationController::class, 'postRegister']);

Route::get('register/confirm/{token}', [RegistrationController::class, 'confirmEmail']);

Route::get('login', [SessionsController::class, 'login'])->middleware('guest');
Route::post('login', [SessionsController::class, 'postLogin'])->middleware('guest');
Route::get('logout', [SessionsController::class, 'logout']);

Route::get('dashboard', ['middleware' => 'auth', function() {
    return view('dashboard', ['greeting' => 'Добро пожаловать, '.Auth::user()->name.'!']);
}]); 